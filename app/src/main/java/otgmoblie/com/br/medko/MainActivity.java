package otgmoblie.com.br.medko;

import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @ViewById(R.id.nav_view)
    NavigationView navigationView;

    @ViewById(R.id.drawer_layout)
    DrawerLayout drawer;

    ActionBarDrawerToggle actionBarDrawerToggle;

    FragmentTransaction fragmentTransaction;



    @Override
    protected void onStart() {
        super.onStart();


        setSupportActionBar(toolbar);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawer,toolbar,R.string.drawer_open,R.string.drawer_close);

        drawer.setDrawerListener(actionBarDrawerToggle);

        actionBarDrawerToggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {int id = item.getItemId();

        if (id == R.id.action_settings) {return true;}

        return super.onOptionsItemSelected(item);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.item_frame1 :
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new Fragment1Activity_());
                fragmentTransaction.commit();
                getSupportActionBar().setTitle("Fragmento 1");
                item.setChecked(true);
                break;

            case R.id.item_frame2 :
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new Fragment2Activity_());
                fragmentTransaction.commit();
                getSupportActionBar().setTitle("Fragmento 2");
                item.setChecked(true);
                break;

            case R.id.item_frame3 :
                fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.main_container, new Fragment3Activity_());
                fragmentTransaction.commit();
                getSupportActionBar().setTitle("Fragmento 3");
                item.setChecked(true);
                break;
        }
        drawer.closeDrawers();
        return true;
    }
}
