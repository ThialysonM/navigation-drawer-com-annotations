package otgmoblie.com.br.medko;

/**
 * Created by thialyson on 23/06/16.
 */
public class ObjetoMenu {

    String nome;

    public ObjetoMenu(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
